import axios from "axios";
const API_URL = 'http://localhost:8000'

export default class dbservice {
    constructor() {
    }

    getQuery(){
        const url = `${API_URL}/api/query`
        return axios.get(url).then(response => response.data)
    }

    getJoinedSet(){
        const url = `${API_URL}/api/joinedset`
        return axios.get(url).then(response => response.data)
    }
}