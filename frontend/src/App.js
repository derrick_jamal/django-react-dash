// App.js
import React, { Component } from 'react';

class App extends Component {
  state = {
    data_table: []
  };
   // This is where the magic happens
  async componentDidMount() {
    try {
      const res = await fetch('http://127.0.0.1:8000/api/query/'); // fetching the data from api, before the page loaded
      const data_table = await res.json();
      this.setState({
        data_table
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div className="visualization-container">
          <div className="nav-bar">
              <div className="title-section">
                  <h6>lobstrio.io</h6>
              </div>
              <div className="company">
                  <h6>Pharma : db_visualization</h6>
              </div>
          </div>
          <div className="row">
              <div className="four columns">
                  <p>MODULE</p>
              </div>
              <div className="four columns">
                  <p>PERIOD</p>
              </div>
              <div className="four columns">
                  <p>VARIABLE</p>
              </div>
          </div>
          <table>
              <thead>
              <tr>
                  <th>ID</th>
                  <th>QUERY TEXT</th>
                  <th>VARIABLE</th>
                  <th>TABLE</th>
                  <th>HAS RATIO</th>
                  <th>TOTAL QUERY TEXT</th>
              </tr>
              </thead>
              <tbody>
              {this.state.data_table.map(item => (
                  <tr>
                      <td>{item.id}</td>
                      <td>{item.query_text}</td>
                      <td>{item.variable}</td>
                      <td>{item.table}</td>
                      <td>{item.has_ratio}</td>
                      <td>{item.total_query_text}</td>
                  </tr>
              ))}
              </tbody>
            </table>
      </div>
    );
  }
}

export default App;