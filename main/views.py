from rest_framework import viewsets
from .serializers import JoinedSerializer, QuerySerializer
from .models import Statistic, Query

class QueryViewSet(viewsets.ModelViewSet):
    serializer_class = QuerySerializer
    queryset = Query.objects.all()

class SQJoinViewSet(viewsets.ModelViewSet):
    serializer_class = JoinedSerializer
    queryset = Statistic.objects.select_related('query')


