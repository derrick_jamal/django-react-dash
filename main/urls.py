from rest_framework import routers
from.views import QueryViewSet, SQJoinViewSet

router = routers.DefaultRouter()

router.register('query', QueryViewSet)
router.register('joinedset', SQJoinViewSet)

urlpatterns = router.urls