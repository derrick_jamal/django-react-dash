from rest_framework import serializers
from .models import Statistic, Query

class QuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Query
        fields = '__all__'


class JoinedSerializer(serializers.Serializer):
    class Meta:
        model = Statistic
        fields = ('id', 'query_text', 'value', 'total', 'ratio')

