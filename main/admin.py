from django.contrib import admin
from .models import Statistic, Query

class StatisticAdmin(admin.ModelAdmin):
    list_display = ['query', 'processing_time', 'module', 'period', 'db_name', 'elapsed', 'status', 'value', 'total', 'ratio']


class QueryAdmin(admin.ModelAdmin):
    list_display = ['query_text', 'variable', 'table', 'has_ratio', 'total_query_text']

admin.site.register(Statistic, StatisticAdmin)
admin.site.register(Query, QueryAdmin)
